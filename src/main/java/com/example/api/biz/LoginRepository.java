package com.example.api.biz;


import com.example.api.model.orm.UserORM;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends CrudRepository<UserORM, Integer> {

    @Query("SELECT username, password FROM UserORM where username = :username and password = :password")
    UserORM findByUsernameAndPassword(@Param("username") String username, @Param("password") String password);
}