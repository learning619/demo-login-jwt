package com.example.api.biz;

import com.example.api.model.orm.UserORM;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserORM, Integer> {

}
