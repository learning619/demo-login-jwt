package com.example.api.biz.service;


import com.example.api.biz.UserRepository;
import com.example.api.model.UserDTO;
import com.example.api.model.orm.UserORM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface UserService {
    List<UserORM> getAllUser();

    void saveUser(UserDTO user);

    void deleteUser(Long id);

    Optional<UserORM> findUserById(Long id);

    @Service
    class UserServiceImpl implements UserService {

        @Autowired
        UserRepository userRepository;

        @Override
        public List<UserORM> getAllUser() {
            return (List<UserORM>) userRepository.findAll();
        }

        @Override
        public void saveUser(UserDTO user) {

        }

        @Override
        public void deleteUser(Long id) {
            userRepository.deleteById(Math.toIntExact(id));
        }

        @Override
        public Optional<UserORM> findUserById(Long id) {
            return userRepository.findById(Math.toIntExact(id));
        }
    }

}
