package com.example.api.biz.service;

import com.example.api.biz.LoginRepository;
import com.example.api.exception.UserNotFoundException;
import com.example.api.model.UserDTO;
import com.example.api.model.orm.UserORM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

public interface LoginService  {
    UserDTO checklogin(String username,String password);

    Map<String, UserDTO> redis = new HashMap();

    @Service
    class LoginServiceImpl implements LoginService{

        @Autowired
        LoginRepository loginRepository;

        @Override
        public UserDTO checklogin(String username, String password) {

            // check user in redis
            // if user has already in redis => return user directly
            if(!redis.isEmpty()){
                UserDTO response = null;
                redis.forEach((s, o) -> {
                    if(s.equals(username) && o.getPassword().equals(password)){
                        LocalTime localTime = LocalTime.now();
                        String session = localTime.getHour() + 2 + ":" + localTime.getMinute() + ":" + localTime.getSecond();
                        response.setUsername(username);
                        response.setPassword(password);
                        response.setSession(session);

                    }
                });
                if(response != null){
                    return response;
                }
            }

            // if not => query database
            UserORM userORM = loginRepository.findByUsernameAndPassword(""+username,""+password);
            if (userORM != null) {


                // add to redis (add expire time = 3000)

                LocalTime localTime = LocalTime.now();
                String session = localTime.getHour() + 2 + ":" + localTime.getMinute() + ":" + localTime.getSecond();
                UserDTO response = new UserDTO(userORM.getUsername(), userORM.getPassword());
                response.setSession(session);

                // put redis

                redis.put(userORM.getUsername(), response);

                return response;
            }

            throw new UserNotFoundException();
        }
    }

}
