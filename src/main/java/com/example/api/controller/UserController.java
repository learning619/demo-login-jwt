package com.example.api.controller;

import com.example.api.biz.service.UserService;
import com.example.api.model.UserDTO;
import com.example.api.model.orm.UserORM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/getAllUser")
    List<UserORM> getAllUser(){

        return userService.getAllUser();
    }
    @PostMapping("/add")
    public UserDTO add(String username , String password) {
        UserDTO user = new UserDTO(username,password);
        userService.saveUser(user);
        return user;
    }

}
