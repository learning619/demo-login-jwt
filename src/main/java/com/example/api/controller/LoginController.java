package com.example.api.controller;

import com.example.api.biz.service.LoginService;
import com.example.api.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/v1")
public class LoginController {

    @Autowired
    LoginService loginService;

    @CrossOrigin(origins = "*")
    @PostMapping("/login")
    public UserDTO Login(String username, String password) {
        return loginService.checklogin(username, password);
    }

}
