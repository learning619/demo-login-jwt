package com.example.api.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDTO {

    Long id;

    String username;

    String password;

    String errMsg;
    String session;

    public UserDTO(String errMsg) {
        this.errMsg = errMsg;
    }

    public UserDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

}
