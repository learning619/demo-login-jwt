package com.example.api.model;

import java.time.Instant;

public class LoginDTO {

    String token; // JWT token

    String refreshToken; // Refresh token

    Instant lastVisit;
}
